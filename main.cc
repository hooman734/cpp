#include <iostream>
#include <vector>
using namespace std;

typedef long T;

vector<T> rotate(vector<T> nums, T k) {

	vector<T> first = {}, second = {}, result = {};
	k = k % nums.size();

	for (auto item = nums.begin() + k; item != nums.end(); ++item) {
		first.insert(first.end(), *item);
	}

	for (auto item = nums.begin(); item != nums.begin() + k; ++item) {
		second.insert(second.end(), *item);
	}

	for (long i : first) {
		result.insert(result.end(), i);
	}

	for (long i : second) {
		result.insert(result.end(), i);
	}

	return result;
}

void print(const vector<T>& inp, const string& title = "") {
	for (long i : inp) {
		cout << title << " - " << i << endl;
	}
}

typedef struct containsStruct {
	bool answer;
	int index;
} C;

C contains(const vector<T>& inp, const T& item) {
	C result;
	for (auto x = 0; x < inp.size(); ++x) {
		if (inp.at(x) == item) {
			result.index = x;
			result.answer = true;
			return result;
		}
	}
	result.index = -1;
	result.answer = false;
	return result;
}

typedef struct eraseStruct {
    vector<T> result;
    int occurrence;
} E;

E erase(const vector<T>& inp, const T item) {
    E e;
    vector<T> result;
    int occurrence = 0;
    result.assign(inp.cbegin(), inp.cend());
    while (contains(result, item).answer) {
    	auto index = contains(result, item).index;
    	result.erase(result.begin() + index);
    	occurrence++;
    }
    e.result = result;
    e.occurrence = occurrence;
    return e;
}

vector<T> unique(vector<T> &nums)
{
	vector<T> temp;
    vector<T> result;

	if (!nums.empty()) {

        temp.assign(nums.cbegin(), nums.cend());
        for (auto item: nums) {
           if (erase(temp, item).occurrence == 1) {
              result.push_back(item);
           }
        }

		return result;
	}
	return result;
}

int main() {
	auto v = vector<T> {1,1,2,2,7,2,2,3,4,5,5,6,2,1};
	auto ans = rotate(v, 4);
    //print(v);
    //v = erase(v, 2);
    //v = erase(v, 5);
//    print(v);
//    auto u = unique(v);
//    print(u);

    auto uq = unique(v);

    print(uq, "unique");
	return 0;
}